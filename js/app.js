$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $(".carousel").carousel({
    interval: 3000,
  });

  $("#exampleModal").on("show.bs.modal", function (e) {
    // do something...
    console.log('show');
    $('button[data-target="#exampleModal"]').css({ 'backgroundColor' : '#cfcfcf', 'color' : 'black'}).prop('disabled', true);
  });

  $("#exampleModal").on("shown.bs.modal", function (e) {
    // do something...
    console.log('shown');
  });

  $("#exampleModal").on("hide.bs.modal", function (e) {
    // do something...
    console.log('hide');
  });

  $("#exampleModal").on("hidden.bs.modal", function (e) {
    // do something...
    console.log('hidden');
    $('button[data-target="#exampleModal"]').css({ 'backgroundColor' : '#f06292', 'color': 'white'}).prop('disabled', false);
  });

  $("#exampleModal").on("hidePrevented.bs.modal", function (e) {
    // do something...
    console.log('hidePrevented');
  });

});
